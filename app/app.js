const express = require('express');
const multer = require('multer');
const path = require ('path');
const config = require('../config');

const storage = multer.diskStorage({
    destination: function (req,file,cb) {
        cb(null,cofig.uploadPath)
    },
    filename: function (req,file,cb) {
        cb(null,path.extname(file.origin))
    }
})